const books_json = [
    {
        "name": "Subconscious Mind Power",
        "description": "By James Thompson",
        "link": "https://play.google.com/books/reader?id=Zh9hMAAAAEAJ",
        "day": "Monday"
    },
    {
        "name": "Subtle Art of Not Giving a F*ck",
        "description": "By Mark Manson",
        "link": "https://play.google.com/books/reader?id=q1qkMAAAAEAJ",
        "day": "Tuesday"
    },
    {
        "name": "Thinking, Fast and Slow",
        "description": "By Daniel Kahneman",
        "link": "https://play.google.com/books/reader?id=r0edMAAAAEAJ",
        "day": "Wednesday"
    },
    {
        "name": "Mindfulness in Plain English",
        "description": "By Bhante Henepola Gunaratana",
        "link": "https://play.google.com/books/reader?id=D6k7LQAAAEAJ",
        "day": "Thursday"
    },
    {
        "name": "Power of Positive Thinking",
        "description": "By Norman Vincent Peale",
        "link": "https://play.google.com/books/reader?id=8iRhMAAAAEAJ",
        "day": "Friday"
    },
    {
        "name": "How to stop worrying and start living",
        "description": "By Dale Carnegie",
        "link": "https://play.google.com/books/reader?id=XyNhMAAAAEAJ",
        "day": "Saturday"
    },
    {
        "name": "5 second rule",
        "description": "By Mel Robbins",
        "link": "https://play.google.com/books/reader?id=FiJhMAAAAEAJ",
        "day": "Sunday"
    }
]
const watchlist_json = [
    {
        "name": "The Office",
        "description": "A is for Apple",
        "image": "images/ofc.jpg",
        "link": "http://www.apple.com",
        "day": "Monday"
    },
    {
        "name": "The Simpsons",
        "description": "",
        "image": "images/simp.jpg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Tuesday"
    },
    {
        "name": "Family Guy",
        "description": "",
        "image": "images/fam.jpg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Wednesday"
    },
    {
        "name": "How I Met Your Mother",
        "description": "",
        "image": "images/how.jpg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Thursday"
    },
    {
        "name": "Person of Interest",
        "description": "",
        "image": "images/poi.jpg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Friday"
    },
    {
        "name": "Silicon Valley",
        "description": "",
        "image": "images/sil.jpg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Saturday"
    },
    {
        "name": "Veep",
        "description": "",
        "image": "images/veep.svg",
        "link": "https://www.netflix.com/in/title/70143836",
        "day": "Sunday"
    }
]


const today = new Date()
const day = today.getDay()
const daylist = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


const watchlist_visuals = document.getElementById("watchlist_visuals")
watchlist_json.filter(e => e.day === daylist[day]).forEach(e => {
    watchlist_visuals.innerHTML += `
        <div class="block p-6 rounded-lg shadow-lg bg-white max-w-sm dark:bg-gray-700" id="">
            <img src="${e.image}" alt="${e.name}">
            <h5 class="text-gray-900 text-xl leading-tight font-medium mb-2 dark:text-gray-100">
            ${e.name}
            </h5>
            <p class="text-gray-700 text-base mb-4 dark:text-gray-200">
            ${e.description}
            </p>
            <button type="button" href="${e.link}"
                class=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                Open
            </button>
        </div>
    `
});



const books_visuals = document.getElementById("books_visuals")
books_json.filter(e => e.day === daylist[day]).forEach(e => {
    books_visuals.innerHTML += `
        <div class="cursor-none block p-6 rounded-lg shadow-lg bg-white max-w-sm dark:bg-gray-700" id="">
            <h5 class="text-gray-900 text-6xl leading-tight font-medium mb-2 dark:text-gray-100">
            ${e.name}
            </h5>
            <p class="text-gray-700 text-base mb-4 dark:text-gray-200">
            ${e.description}
            </p>
            <a target="_blank" href="${e.link}"
                class=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
                Read
            </a>
        </div>
    `
});